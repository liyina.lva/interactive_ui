# -*- coding: utf-8 -*-
from order import Order


class OrderConcrete(Order):

    def execute_order_a(self):
        print('Order a')

    def execute_order_b(self):
        print('Order b')

    def execute_oder_c(self):
        print('Order c')
