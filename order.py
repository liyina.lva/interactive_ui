# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Order(object):
    __metaclass__ = ABCMeta

    def execute_order_a(self):
        pass

    def execute_order_b(self):
        pass

    def execute_oder_c(self):
        pass
