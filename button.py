# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Button(object):
    __metaclass__ = ABCMeta

    def __init__(self, command):
        self._command = command

    @abstractmethod
    def press(self):
        self._command.execute()
