# -*- coding: utf-8 -*-
from abc import ABCMeta


class FormOrder(object):
    __metaclass__ = ABCMeta

    def __init__(self, order):
        self._command = order

    def getCommand(self):
        return self._command
