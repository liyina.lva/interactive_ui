# -*- coding: utf-8 -*-
from button import Button
from command import Command
from command_order_a import CommandOrderA
from form_order import FormOrder
from order import Order
from order_concrete import OrderConcrete

if __name__ == "__main__":

    order_concrete = OrderConcrete()
    order_a = CommandOrderA(order_concrete)
    formOrder = FormOrder(order_a)
    onCommand = formOrder.getCommand()
    onPressed = Button(onCommand)
    onPressed.press()
