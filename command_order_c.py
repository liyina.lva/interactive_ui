# -*- coding: utf-8 -*-
from command import Command


class CommandOrderC(Command):

    def __init__(self, order_concrete):
        self._order_concrete = order_concrete

    def execute(self):
        self._order_concrete.execute_order_c()
